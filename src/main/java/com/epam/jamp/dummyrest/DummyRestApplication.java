package com.epam.jamp.dummyrest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class DummyRestApplication {

    public static void main(String[] args) {
        SpringApplication.run(DummyRestApplication.class, args);
    }
}
