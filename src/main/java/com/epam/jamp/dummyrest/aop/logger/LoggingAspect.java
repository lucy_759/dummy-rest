package com.epam.jamp.dummyrest.aop.logger;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LoggingAspect {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoggingAspect.class);

    @Pointcut("execution(* com.epam.jamp.dummyrest.service..*.*(..))")
    private void repositoryMethodPointcut() { };


    @Around("repositoryMethodPointcut()")
    private Object logRetrivingUsersFromDb(ProceedingJoinPoint point) throws Throwable {
        long start = System.currentTimeMillis();
        point.proceed();
        long end = System.currentTimeMillis();
        LOGGER.info("{}#{}() execution time is {} milliseconds", point.getSignature().getDeclaringTypeName(), point.getSignature().getName(), end - start);
        return point.proceed();
    }
}
