package com.epam.jamp.dummyrest.controller;

import com.epam.jamp.dummyrest.model.LoginForm;
import com.epam.jamp.dummyrest.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/help")
public class HelpPageController {

    @Autowired
    private LoginService loginService;

    @GetMapping
    public ModelAndView login() {
        ModelAndView mav = new ModelAndView("admin-login");
//        mav.addObject("cred_mistake", true);
        return mav;
    }

    @PostMapping("/login")
    public String greetingSubmit(@ModelAttribute LoginForm form) {
        System.out.println(228);
        return "help-page";
    }
}
