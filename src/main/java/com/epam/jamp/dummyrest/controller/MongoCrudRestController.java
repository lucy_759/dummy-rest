package com.epam.jamp.dummyrest.controller;

import com.epam.jamp.dummyrest.model.User;
import com.epam.jamp.dummyrest.service.MongoCrudService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.concurrent.CompletableFuture;

/**
 * Created by Marko on 11.09.2017.
 */
@RestController
@RequestMapping("/rest-api")
public class MongoCrudRestController {

    @Autowired
    private MongoCrudService mongoCrudService;

    public MongoCrudRestController() {
    }

    @RequestMapping("/future-user/{userId}")
    public CompletableFuture<User> getById(@PathVariable("userId") String userId) {
        CompletableFuture<User> future = new CompletableFuture<>();
        return future.supplyAsync(() -> mongoCrudService.getById(userId));
    }

    @Async("threadPoolTaskExecutor")
    @RequestMapping("/async-user/{userId}")
    public CompletableFuture<User> getByIdAsync(@PathVariable("userId") String userId) {
        CompletableFuture<User> future = new CompletableFuture<>();
        return future.supplyAsync(() -> mongoCrudService.getById(userId));
    }

    @GetMapping("/users")
    @ResponseBody
    public List<User> getAll() {
        return mongoCrudService.getAll();
    }


    @PostMapping(path = "/create-user")
    @ResponseStatus(HttpStatus.CREATED)
    public void createUser(@RequestBody @Valid User user) {
        mongoCrudService.create(user);
    }

    @PutMapping(path = "/user/{userId}")
    @ResponseStatus(HttpStatus.OK)
    public void update(@PathVariable("userId") String userId, @RequestBody @Valid User user) {
        mongoCrudService.updateById(userId, user);
    }

    @DeleteMapping(path = "/user/{userId}")
    @ResponseStatus(HttpStatus.OK)
    public User deleteUser(@PathVariable("userId") String userId) {
        return mongoCrudService.deleteById(userId);
    }

    @GetMapping(path = "/exception")
    public void throwAnException() throws Exception {
        throw new Exception("Exception msg");
    }

}
