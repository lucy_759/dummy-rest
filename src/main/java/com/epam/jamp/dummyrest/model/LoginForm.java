package com.epam.jamp.dummyrest.model;

/**
 * Created by Marko on 29.09.2017.
 */
public class LoginForm {
    private String username;
    private String pass;

    public LoginForm(String username, String pass) {
        this.username = username;
        this.pass = pass;
    }

    public LoginForm() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }
}
