package com.epam.jamp.dummyrest.persistance;

import com.epam.jamp.dummyrest.model.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.BasicDBObject;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.mongodb.client.model.Filters.eq;

/**
 * Created by Marko on 12.09.2017.
 */
@Repository
public class UserRepository {
    private static final String ID = "id";
    @Autowired
    private MongoDatabase db;
    @Autowired
    private ObjectMapper mapper;

    private String userCollection;

    @Cacheable("user")
    public User getById(String id) {
        MongoCollection<Document> collection = db.getCollection(userCollection);
        FindIterable<Document> documents = collection.find((eq(ID, id)));
        return new ObjectMapper().convertValue(documents.first(), User.class);
    }

    public List<User> getAllUsers() {
        List<User> users = new ArrayList<>();
        MongoCollection<Document> collection = db.getCollection(userCollection);
        FindIterable<Document> documents = collection.find();
        documents.iterator().forEachRemaining(d -> users.add(mapper.convertValue(d, User.class)));
        return users;
    }

    public void create(User user) {
        MongoCollection<Document> collection = db.getCollection(userCollection);
        Document document = new Document(mapper.convertValue(user, Map.class));
        collection.insertOne(document);
    }

    public User deleteById(String id) {
        MongoCollection<Document> collection = db.getCollection(userCollection);
        Document document = collection.findOneAndDelete(eq(ID, id));
        return mapper.convertValue(document, User.class);
    }

    public void update(String id, User user) {
        MongoCollection<Document> collection = db.getCollection(userCollection);
        collection.replaceOne(eq(ID, id), mapper.convertValue(user, Document.class));
    }

    public void clearCollection() {
        MongoCollection<Document> collection = db.getCollection(userCollection);
        collection.deleteMany(new Document());
    }

    @Value("${mongodb.collection}")
    public void setUserCollection(String userCollection) {
        this.userCollection = userCollection;
    }

}
