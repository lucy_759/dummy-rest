package com.epam.jamp.dummyrest.service;

/**
 * Created by Marko on 29.09.2017.
 */
public interface LoginService {
    boolean login(String username, String pass);
}
