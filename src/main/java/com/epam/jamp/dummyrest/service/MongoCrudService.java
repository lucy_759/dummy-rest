package com.epam.jamp.dummyrest.service;

import com.epam.jamp.dummyrest.model.User;

import java.util.List;

/**
 * Created by Marko on 11.09.2017.
 */
public interface MongoCrudService {

    public void create(User user);

    public User getById(String id);

    public List<User> getAll();

    public User deleteById(String id);

    public void updateById(String id, User user);

}
