package com.epam.jamp.dummyrest.service.impl;

import com.epam.jamp.dummyrest.service.LoginService;
import org.springframework.stereotype.Service;

/**
 * Created by Marko on 29.09.2017.
 */
@Service
public class DummyLoginService implements LoginService {

    private static final String USERNAME = "username";
    private static final String PASSWORD = "password";

    @Override
    public boolean login(String username, String pass) {
        return USERNAME.equals(username) && PASSWORD.equals(pass);
    }
}
