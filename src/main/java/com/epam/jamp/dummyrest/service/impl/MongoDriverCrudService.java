package com.epam.jamp.dummyrest.service.impl;

import com.epam.jamp.dummyrest.model.User;
import com.epam.jamp.dummyrest.persistance.UserRepository;
import com.epam.jamp.dummyrest.service.MongoCrudService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.IdGenerator;

import java.util.List;

/**
 * Created by Marko on 11.09.2017.
 */
@Service
public class MongoDriverCrudService implements MongoCrudService {

    @Autowired
    private IdGenerator idGenerator;

    @Autowired
    private UserRepository userRepository;

    @Override
    public void create(User user) {
        user.setId(idGenerator.generateId().toString());
        userRepository.create(user);
    }

    @Override
    public User getById(String id) {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return userRepository.getById(id);
    }

    @Override
    public List<User> getAll() {
        return userRepository.getAllUsers();
    }

    @Override
    public User deleteById(String id) {
        return userRepository.deleteById(id);
    }

    @Override
    public void updateById(String id, User user) {
        userRepository.update(id, user);
    }

    public void setIdGenerator(IdGenerator idGenerator) {
        this.idGenerator = idGenerator;
    }

    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }
}
