package com.epam.jamp.dummyrest.validation;

import com.epam.jamp.dummyrest.model.User;
import com.epam.jamp.dummyrest.validation.annotation.UserMandatoryFields;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Objects;

/**
 * Created by Marko on 25.09.2017.
 */
public class UserMandatoryUserFieldsValidator implements ConstraintValidator<UserMandatoryFields, User> {

    private static final String IP_ADDRESS_REGEX = "(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)";

    @Override
    public void initialize(UserMandatoryFields constraintAnnotation) {

    }

    @Override
    public boolean isValid(User value, ConstraintValidatorContext context) {
        return Objects.nonNull(value) &&
                Objects.nonNull(value.getFirstName()) &&
                Objects.nonNull(value.getLastName()) &&
                Objects.nonNull(value.getEmail()) &&
                Objects.nonNull(value.getIpAddress()) &&
                value.getIpAddress().matches(IP_ADDRESS_REGEX);
    }

}
