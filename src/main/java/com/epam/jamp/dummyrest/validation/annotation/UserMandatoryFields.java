package com.epam.jamp.dummyrest.validation.annotation;

import com.epam.jamp.dummyrest.validation.UserMandatoryUserFieldsValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by Marko on 25.09.2017.
 */
@Constraint(validatedBy = UserMandatoryUserFieldsValidator.class)
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface UserMandatoryFields {

    String message() default "User has missing mandatory fields";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
