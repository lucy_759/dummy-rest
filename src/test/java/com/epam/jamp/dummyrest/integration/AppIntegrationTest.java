package com.epam.jamp.dummyrest.integration;

import com.epam.jamp.dummyrest.DummyRestApplication;
import com.epam.jamp.dummyrest.model.User;
import com.epam.jamp.dummyrest.persistance.UserRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Created by Marko on 28.09.2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DummyRestApplication.class)
@WebAppConfiguration
public class AppIntegrationTest {

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private UserRepository repository;

    @Autowired
    private ObjectMapper mapper;

    private MockMvc mockMvc;

    private User firstUser;

    private User secondUser;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
        repository.clearCollection();
        firstUser = new User("1", "First1", "Last1", "email1@email.com", "Male", "127.0.0.1");
        secondUser = new User("2", "First2", "Last2", "email2@email.com", "Female", "127.0.0.2");
        repository.create(firstUser);
        repository.create(secondUser);
    }

    @Test
    public void testUserCreation() throws Exception {
        User newUser = new User("3", "First3", "Last3", "email3@email.com", "Male", "127.0.0.3");
        mockMvc.perform(post("/rest-api/create-user")
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertUserToJson(newUser)))
                .andExpect(status().isCreated());
    }

    @Test
    public void testUserRemoving() throws Exception {
        mockMvc.perform(delete("/rest-api/user/" + firstUser.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.id", is(firstUser.getId())))
                .andExpect(jsonPath("$.firstName", is(firstUser.getFirstName())))
                .andExpect(jsonPath("$.lastName", is(firstUser.getLastName())))
                .andExpect(jsonPath("$.email", is(firstUser.getEmail())))
                .andExpect(jsonPath("$.ipAddress", is(firstUser.getIpAddress())));
    }

    @Test
    public void testGettingAllUsers() throws Exception {
        mockMvc.perform(get("/rest-api/users"))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id", is(firstUser.getId())))
                .andExpect(jsonPath("$[0].firstName", is(firstUser.getFirstName())))
                .andExpect(jsonPath("$[0].lastName", is(firstUser.getLastName())))
                .andExpect(jsonPath("$[0].email", is(firstUser.getEmail())))
                .andExpect(jsonPath("$[0].ipAddress", is(firstUser.getIpAddress())))
                .andExpect(jsonPath("$[1].id", is(secondUser.getId())))
                .andExpect(jsonPath("$[1].firstName", is(secondUser.getFirstName())))
                .andExpect(jsonPath("$[1].lastName", is(secondUser.getLastName())))
                .andExpect(jsonPath("$[1].email", is(secondUser.getEmail())))
                .andExpect(jsonPath("$[1].ipAddress", is(secondUser.getIpAddress())));
    }

    @Test
    public void testUpdatingUser() throws Exception {
        User userWithUpdatedFields = new User("1", "First3", "Last3", "email3@email.com", "Male", "127.0.0.3");
        mockMvc.perform(put("/rest-api//user/" + firstUser.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(convertUserToJson(userWithUpdatedFields)))
                .andExpect(status().isOk());
        User userFromRepo = repository.getById(firstUser.getId());
        Assert.assertEquals("User object should be the same", userWithUpdatedFields, userFromRepo);
    }

    private byte[] convertUserToJson(User user) throws JsonProcessingException {
        return mapper.writeValueAsBytes(user);
    }

}
